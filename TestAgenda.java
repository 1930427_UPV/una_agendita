import java.util.*;

public class TestAgenda
{
    
    public static void main(String... args){
        
        Scanner sc;
        sc = new Scanner(System.in);
        char op;
        int n =0;
        int t=0;
        int sum=0;
        boolean ban_1= false; // Pa verificar si se guarda o no
        String opcion; //pal switch
        //Agregar contactos
        String nombre;
        Date fdn;
        String email;
        long telefono;
        int num=0;
        String fecha;
        StringTokenizer st=null;
        Agenda obj = new Agenda();
        Contacto contacto,tmp;
        tmp = null;
        int nMax = obj.agenda.size() - 1; // numero indice para la agenda
        System.out.println("CONTACTOS EXISTENTES");
        for(sum=n; sum<obj.agenda.size(); sum++){
            System.out.println(num+ " " +obj.navegarAgenda(sum).toString());
           num+=1;
            }
            
        do
         {
            System.out.println("|||||||||||||||||||||||MENU|||||||||||||||||||||");
            System.out.println("a ->Nuevo contacto");
            System.out.println("b ->Buscar contacto");
            System.out.println("c ->Borrar contacto");
            System.out.println("d ->Editar contacto");
            System.out.println("e ->Mostrar contactos");
            System.out.println("s ->salir");
            opcion=sc.nextLine();
            
            switch (opcion)
            {
              case"a":
              
              System.out.println ("Nuevo contacto:");
              System.out.println ("Nombre:");
              nombre = sc.nextLine();
              System.out.print("Fecha de nacimiento (yyyy mm dd): ");
              fecha = sc.nextLine();
              st = new StringTokenizer(fecha, " ");
              fdn = new Date((Integer.parseInt(st.nextElement().toString())-1900), (Integer.parseInt(st.nextElement().toString())-1), Integer.parseInt(st.nextElement().toString()));
              System.out.println ("email:");
              email = sc.nextLine();
              System.out.println ("Numero de telefono:");
              telefono = Long.valueOf(sc.nextLine());
              contacto = new Contacto (nombre,fdn, email, telefono);
              obj.agregarContacto(contacto);
              ban_1 = obj.guardarAgenda();
              System.out.println ("Guardando en la lista de contactos... " + ban_1);
              nMax++;
              break;
              case"b": 
              try{
                    System.out.println("Ingrese el número de teléfono del contacto");
                    telefono=sc.nextLong();
                    sc.nextLine();
                    tmp = obj.buscarContacto(telefono);
                    System.out.println("El contacto que busca: "+tmp.toString());
                    }
                    catch (NullPointerException e){
                    System.out.println("No se encontró ese contacto");
                    }
                    catch(Exception e){
                    e.printStackTrace();
                    } 
              break;
              
              case "c":
              
              System.out.println("Ingrese la posición del número que desee eliminar");
              num=sc.nextInt();
              
              System.out.println("¿Desea eliminarlo? Sí=y, No=N");
              sc.nextLine();
              opcion=sc.nextLine(); 
              //opcion="y";
              if(opcion.equals("y")){
                  
                  obj.agenda.remove(num);
                  nMax--;
                  ban_1 = obj.guardarAgenda();
                  System.out.println("CONTACTO ELIMINADO");
                  System.out.println("");
                    }
              break;
              
              case "d":
              System.out.println("Ingrese la posición del número que desee modificar");
              num=sc.nextInt();
              sc.nextLine();
              System.out.println("¿Desea modificar? Sí=y, No=N");
              opcion=sc.nextLine();
              if(opcion.equals("y")){
                  obj.agenda.remove(num);
                  System.out.println ("Nombre:");
                  nombre = sc.nextLine();
                  System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                  fecha = sc.nextLine();
                  st = new StringTokenizer(fecha, " ");
                  fdn = new Date((Integer.parseInt(st.nextElement().toString())-1900), (Integer.parseInt(st.nextElement().toString())-1), Integer.parseInt(st.nextElement().toString()));
                  System.out.println ("email:");
                  email = sc.nextLine();
                  System.out.println ("Numero de telefono:");
                  telefono = Long.valueOf(sc.nextLine());
                  contacto = new Contacto (nombre,fdn, email, telefono);
                  obj.ModificarContacto(num, contacto);
                  ban_1 = obj.guardarAgenda();                  
                  System.out.println("CONTACTO MODIFICADO...");
                  System.out.println("");  
                }
              break;
              
              case "e":
              num=0;
              for(sum=n; sum<obj.agenda.size(); sum++){
                System.out.println(num+ " " +obj.navegarAgenda(sum).toString());
                num+=1;
              }
              break;
              
              case "s":
              System.out.println("Adiós");
              opcion="s";
              break;
            }
            }while(opcion != "s");
            ban_1 =  obj.guardarAgenda();
        }
        
    }
/*
        finally{
            ban_1 =  obj.guardarAgenda();
            System.out.println("GUardando lista de contactos: "+ban_1);
            // Si muestra TRUE se guardo la lista a un archivo.
            //Si muestra FALSE hubo una excepcion y no se guardo la lista.
        }
        */
        
